grav = 0.2;
hsp = 0;
vsp = 0;
movespeed = 4;
image_speed = 0.3;

jumpspeed_normal = 7;
jumpspeed_powerup = 10;
isShot = false; //Are we shooting?
isSlide = false;
canMove = true;
canSpriteChange = true;

//Physics variables
slideSpeed = 2.5;
slideFrames = 26;


jumpspeed = jumpspeed_normal;

//Sprites

spriteStandShot = spr_stand_shot;
spriteJumpShot = spr_jump_shot;
spriteWalkShot = spr_walk_shot;
spriteStand = spr_stand;
spriteRun = spr_player_run;
spriteSlide = spr_player_slide;
spriteJump = spr_player_jump;
spriteFall = spr_player_fall;


//IA Checks
hcollision = false;
fearofheights = false;

//Constants
grounded = false;
jumping = false;


//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 4;
hsp_jump_applied = 0;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;

//Init variables
key_left = 0;
key_right = 0;
key_jump = false;
